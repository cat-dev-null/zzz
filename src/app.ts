import DI from "./core/di.ts";
import BasicAuthAuthorizer from "./core/modules/authentication/basicAuth.ts";
import BearerTokenAuthorizer from "./core/modules/authentication/bearerToken.ts";
import HeaderAuthorizer from "./core/modules/authentication/header.ts";
import FileStorage from "./core/stores/storages/files/mod.ts";
import QueryAuthorizer from "./core/modules/authentication/query.ts";
import StoragesStore from "./core/stores/storage.ts";
import ConfigStorage from "./core/stores/storages/config/mod.ts";
import { Command } from "./core/deps.ts";
import { Scope } from "./core/modules/scope/mod.ts";
import { Collection, HttpRequest } from "./core/modules/requests/mod.ts";
import { Context } from "./core/modules/context/mod.ts";
import { Authorization } from "./core/modules/authentication/mod.ts";
import { Cookies } from "./core/modules/cookies/mod.ts";

const APP_VERSION = "0.85.0";

// deno-fmt-ignore
export function initDi(): void{
  DI.register("IAuthorizer",            () => new BasicAuthAuthorizer());
  DI.register("IAuthorizer",            () => new BearerTokenAuthorizer());
  DI.register("IAuthorizer",            () => new HeaderAuthorizer());
  DI.register("IAuthorizer",            () => new QueryAuthorizer());
  DI.register("IStore",                 () => new StoragesStore());
  DI.register(`IStorage:${Scope.name}`,         () => new ConfigStorage('zzz.yml', 'yml'));
  DI.register(`IStorage:${HttpRequest.name}`,   () => new FileStorage('requests', 'yml'));
  DI.register(`IStorage:${Collection.name}`,    () => new FileStorage('requests', 'yml'));
  DI.register(`IStorage:${Context.name}`,       () => new FileStorage('requests/_contexts', 'yml'));
  DI.register(`IStorage:${Authorization.name}`, () => new FileStorage('requests/_auth', 'yml'));
  DI.register(`IStorage:${Cookies.name}`,       () => new FileStorage('requests/_cookies', 'yml'));
}

export default function createAppCommand(): Command {
  const cmd = new Command();
  cmd.name("zzz").version(APP_VERSION).description("Replacement for Postman with Desktop, Web, CLI, TUI, and HTTP interfaces");
  cmd.globalOption("--trace", "Enable trace logging", { default: false });
  return cmd;
}
