import { Trace } from "../etc.ts";
import DI from "../di.ts";
import { IStorage, IStore, Model, SearchParams } from "./mod.ts";
import { Authorization } from "../modules/authentication/mod.ts";
import { Context } from "../modules/context/mod.ts";
import { Cookies } from "../modules/cookies/mod.ts";
import { Collection, HttpRequest } from "../modules/requests/mod.ts";
import { Scope } from "../modules/scope/mod.ts";
import FileStorage from "./storages/files/mod.ts";

export default class StoragesStore implements IStore {
  private storages = new Map<string, IStorage>();
  constructor() {
    [HttpRequest.name, Collection.name, Scope.name, Authorization.name, Context.name, Cookies.name].forEach((modelName: string) => {
      this.addStorage(modelName, DI.newInstance("IStorage:" + modelName) as IStorage);
    });
  }
  addStorage(modelType: string, storage: IStorage): void {
    this.storages.set(modelType, storage);
  }
  async getModelType(id: string): Promise<string> {
    Trace(`FileStore: Getting model type for ID: ${id}`);
    for (const modelType of this.storages.keys()) {
      if (await this.storages.get(modelType)!.has(id)) {
        return modelType;
      }
    }
    throw new Error("No Model found with ID: " + id);
  }
  get(modelType: string, id: string): Promise<Model> {
    Trace(`FileStore: Getting model of type ${modelType}, id ${id}`);
    return this.storage(modelType).retrieve(id);
  }
  make(modelType: string, model: Model): Promise<void> {
    return this.storage(modelType).has(model.Id).then((exists) => {
      if (exists) {
        return Promise.reject(`${model.Id} already exists as ${modelType}`);
      } else {
        return this.storage(modelType).save(model);
      }
    });
  }
  put(modelType: string, model: Model): Promise<void> {
    return this.storage(modelType).save(model);
  }
  remove(modelType: string, id: string): Promise<void> {
    return this.storage(modelType).delete(id);
  }
  list(modelType: string): Promise<string[]> {
    Trace("Listing " + modelType);
    return this.storage(modelType).list();
  }
  async search(searchParams: SearchParams): Promise<Model[]> {
    let result = [] as Model[];
    for (const modelType of this.storages.keys()) {
      const searchResults = await this.storage(modelType).search(searchParams);
      result = [...result, ...searchResults];
    }
    return result;
  }
  move(modelType: string, oldId: string, newId: string): Promise<void> {
    return this.storages.get(modelType)!.rename(oldId, newId);
  }
  private storage(modelName: string): IStorage {
    if (!this.storages.get(modelName)) {
      throw new Error("Unknown Model type: " + modelName);
    }
    return this.storages.get(modelName)!;
  }
}

// ----------------------------------------- TESTS -----------------------------------------

import { describe, it } from "../tests.ts";

describe("FileStore", () => {
  describe("getModelType", () => {
    it("works", async () => {
      // fail("Write this test");
    });
  });
  describe("get", () => {
    it("works", async () => {
      // fail("Write this test");
    });
  });
  describe("set", () => {
    it("works", async () => {
      // fail("Write this test");
    });
  });
  describe("list", () => {
    it("works", async () => {
      // fail("Write this test");
    });
  });
  describe("search", () => {
    it("works", async () => {
      // fail("Write this test");
    });
  });
  describe("move", () => {
    it("works", async () => {
      // fail("Write this test");
    });
  });
  describe("storage", () => {
    it("works", async () => {
      // fail("Write this test");
    });
  });
});
