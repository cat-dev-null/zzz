import { Action, asAny, StringToStringMap, Trace } from "../../etc.ts";
import { Feature, IModuleFields, IModuleModifier, Module } from "../../module.ts";
import { Model } from "../../stores/mod.ts";

export class DocumentationModule extends Module implements IModuleFields {
  Name = "Documentation";
  dependencies = [];
  fields = {
    HttpRequest: DocumentationFields,
  };
}

class DocumentationFields {
  Documentation?: Documentation;
}

export class Documentation {
  Description?: string;
  Tags?: string[];
  Parameters?: StringToStringMap;
  Response?: string;
  Comments?: string;
}
