import ExecuteActor from "../core/actors/execute.ts";
import IApplication, { executeHooks, executeModules, FeatureFlags } from "../core/app.ts";
import { extname } from "../core/deps.ts";
import { Action, Log, StringToStringMap, Trace } from "../core/etc.ts";
import { Context } from "../core/modules/context/mod.ts";
import { HttpRequest } from "../core/modules/requests/mod.ts";
import { Scope } from "../core/modules/scope/mod.ts";
import { getFileFormat } from "../core/stores/storages/files/formats.ts";
import { Model } from "../core/stores/mod.ts";
import { OptionsResponse } from "../web/components/Utils.axios.ts";
import Application from "./app.ts";

const DEFAULT_FILETYPE = "json";
const STANDARD_HEADERS = { "Content-Type": "application/json", "Access-Control-Allow-Origin": "*", "Access-Control-Allow-Headers": "allow,content-type,x-zzz-context,x-zzz-scope", "Access-Control-Allow-Methods": "OPTIONS,GET,POST,PUT,PATCH,DELETE" };

interface HttpError {
  statusCode: number;
  message: string;
  stacktrace?: string;
}

function respondToError(error: Error | string, request: Request): Promise<Response> {
  const parts = dissectRequest(request);
  if (error instanceof Error) {
    console.error("Error", error);
    return Promise.resolve(newResponse(
      error.message.includes("No Model found") ? 404 : 400,
      stringifyBody({ message: error.message }, parts.extension || DEFAULT_FILETYPE),
      STANDARD_HEADERS,
    ));
  } else {
    return Promise.resolve(newResponse(500, { message: error }, STANDARD_HEADERS));
  }
}

export function listen(server: IServer): Promise<void> {
  Trace("Starting server on port " + server.port);
  Deno.serve(
    { port: server.port },
    (request: Request): Promise<Response> => {
      Trace("Server:Respond: Responding to " + request.method, request.url);
      let response = null;
      switch (request.method) {
        case "GET":
          return server.respondToGet(request).catch((error) => respondToError(error, request));
        case "POST":
          return server.respondToPost(request).catch((error) => respondToError(error, request));
        case "PUT":
          return server.respondToPut(request).catch((error) => respondToError(error, request));
        case "DELETE":
          return server.respondToDelete(request).catch((error) => respondToError(error, request));
        case "PATCH":
          return server.respondToPatch(request).catch((error) => respondToError(error, request));
        case "OPTIONS":
          return server.respondToOptions(request).catch((error) => respondToError(error, request));
        default:
          return Promise.resolve(newResponse(400, stringifyBody({ message: "Unsupported request method: " + request.method }), STANDARD_HEADERS));
      }
      return response;
    },
  );
  Trace("Server started (asynchronously)");
  return Promise.resolve();
}

export interface IServer {
  port: number;
  app: IApplication;
  respondToGet(request: Request): Promise<Response>;
  respondToPost(request: Request): Promise<Response>;
  respondToPut(request: Request): Promise<Response>;
  respondToPatch(request: Request): Promise<Response>;
  respondToDelete(request: Request): Promise<Response>;
  respondToOptions(request: Request): Promise<Response>;
}

export class Server implements IServer {
  port: number;
  app: Application;
  allowPatch: boolean;
  constructor(app: Application, allowPatch: boolean) {
    this.port = app.argv!.http || 8000;
    this.app = app;
    this.allowPatch = allowPatch;
  }
  async respondToGet(request: Request): Promise<Response> {
    const parts = dissectRequest(request);
    const { pathname } = new URL(request.url);
    Trace("respondToGet " + parts.entityId);
    if (pathname === "/favicon.ico") {
      return this.respondToFavicon();
    }
    return this.executeGet(request).then((model) => {
      return Promise.resolve(newResponse(200, stringifyBody(model, parts.extension || DEFAULT_FILETYPE), STANDARD_HEADERS));
    });
  }
  private respondToFavicon(): Promise<Response> {
    return Promise.resolve(newResponse(204, null, STANDARD_HEADERS));
  }
  respondToPut(request: Request): Promise<Response> {
    Trace("Responding to PUT");
    const parts = dissectRequest(request);
    if (!parts.entityId) {
      return Promise.resolve(newResponse(400, stringifyBody({ message: "Missing entity ID" }), STANDARD_HEADERS));
    }
    return this.executePut(request).then(() => {
      return Promise.resolve(newResponse(204, null, STANDARD_HEADERS));
    });
  }
  respondToPost(request: Request): Promise<Response> {
    Trace("Responding to POST");
    const parts = dissectRequest(request);
    if (!parts.entityId) {
      return Promise.resolve(newResponse(400, stringifyBody({ message: "Missing entity ID" }), STANDARD_HEADERS));
    }
    return this.executePost(request).then(() => {
      return Promise.resolve(newResponse(201, stringifyBody({ message: `Created ${parts.type} ${parts.entityId}` }), STANDARD_HEADERS));
    }).catch((error) => {
      console.error("Error: " + error);
      return Promise.resolve(newResponse(400, stringifyBody({ message: error }), STANDARD_HEADERS));
    });
  }
  async respondToPatch(request: Request): Promise<Response> {
    if (!this.allowPatch) {
      return Promise.resolve(newResponse(405, stringifyBody({ message: "PATCH not supported" }, DEFAULT_FILETYPE), STANDARD_HEADERS));
    }
    const parts = dissectRequest(request);
    Trace("Responding to PATCH");
    const model = await this.executeGet(request);
    if (!("Method" in model)) {
      return Promise.resolve(newResponse(400, stringifyBody({ message: "PATCH only supported for Requests" }, parts.extension || DEFAULT_FILETYPE), STANDARD_HEADERS));
    }
    return this.executeExecute(model, parts.extension);
  }
  async respondToDelete(request: Request): Promise<Response> {
    Trace("Responding to DELETE");
    const parts = dissectRequest(request);
    return this.executeDelete(request).then(() => {
      return Promise.resolve(newResponse(204, stringifyBody(null, parts.extension || DEFAULT_FILETYPE), STANDARD_HEADERS));
    });
  }
  async respondToOptions(request: Request): Promise<Response> {
    Trace("Responding to OPTIONS");
    const headers = {
      "Allow": ["OPTIONS", "GET"],
      ...STANDARD_HEADERS,
    };
    if (request.url !== "/" && this.allowPatch) {
      headers.Allow.push("PATCH");
    }
    const body = await this.getList();
    console.log("BODY", body);
    return Promise.resolve(
      new Response(stringifyBody(body, DEFAULT_FILETYPE), {
        status: 200,
        headers: headers as unknown as HeadersInit,
      }),
    );
  }
  async getList(): Promise<OptionsResponse> {
    const scopes = await this.app.store.list(Scope.name);
    Trace("Scopes:", scopes);
    const contexts = await this.app.store.list(Context.name);
    Trace("Context IDs:", contexts);
    return {
      scopes: scopes,
      contexts: contexts,
    };
  }
  private async executeExecute(model: Model, responseFormat: string): Promise<Response> {
    const action = new Action(this.app.argv as FeatureFlags, this.app.env);
    try {
      await executeHooks("Before", model, action);
      const executeResponse = await (new ExecuteActor()).act(model);
      await executeHooks("After", model, executeResponse);
      return Promise.resolve(newResponse(200, stringifyBody(executeResponse, responseFormat || DEFAULT_FILETYPE), STANDARD_HEADERS));
    } catch (error) {
      return newResponse(599, stringifyBody({ message: error.message }, responseFormat || DEFAULT_FILETYPE), STANDARD_HEADERS);
    }
  }
  private executeGet(request: Request): Promise<Model> {
    const parts = dissectRequest(request);
    const flagValues = this.app.argv as FeatureFlags;
    Trace("Flag values:", flagValues);
    flagValues.context = parts.context || flagValues.context;
    flagValues.scope = parts.scope || flagValues.scope || this.app.env["ZZZ_SCOPE"];
    const action = new Action(flagValues, this.app.env);
    const modelId = getModelIdFromRequest(request);
    const model = { Id: modelId } as Model;
    return executeModules(this.app.modules, action, model)
      .then(() => Promise.resolve(model))
      .catch((error) => {
        Log("Error in executeModules:", error);
        return Promise.reject(error);
      });
  }
  private async executePost(request: Request): Promise<void> {
    const model = await request.json() as Model;
    const { entityId } = dissectRequest(request);
    model.Id = entityId;
    const modelType = getModelTypeForNewRequest(request);
    return this.app.store.make(modelType, model);
  }
  private async executePut(request: Request): Promise<void> {
    const model = await request.json() as Model;
    const { entityId } = dissectRequest(request);
    model.Id = entityId;
    try {
      const modelType = await this.app.store.getModelType(model.Id);
      return this.app.store.put(modelType, model);
    } catch (error) {
      console.log("Reject", error);
      return Promise.reject(error);
    }
  }
  private async executeDelete(request: Request): Promise<void> {
    const modelId = getModelIdFromRequest(request);
    const modelType = await this.app.store.getModelType(modelId);
    return this.app.store.remove(modelType, modelId);
  }
}

function getModelTypeForNewRequest(request: Request): string {
  const fromUrl = (new URL(request.url)).searchParams.get("type");
  if (!fromUrl || fromUrl === "request") {
    return HttpRequest.name;
  } else {
    return fromUrl;
  }
}
// deno-lint-ignore no-explicit-any
function stringifyBody(result: any, fileExtension = DEFAULT_FILETYPE): string | null {
  if (result === null) {
    return null;
  }
  return getFileFormat(fileExtension).stringify(result);
}

// deno-lint-ignore no-explicit-any
function newResponse(status: number, body: any, headers: StringToStringMap): Response {
  return new Response(body, { status, headers });
}
function getModelIdFromRequest(request: Request): string {
  const parts = dissectRequest(request);
  let result = parts.entityId;
  if (parts.extension) {
    result = parts.entityId.substring(0, parts.entityId.indexOf("."));
  }
  return result;
}
function dissectRequest(request: Request): RequestParts {
  const { searchParams, pathname } = new URL(request.url);
  const extension = extname(pathname).replace("\.", "");
  const decodedPathname = decodeURI(pathname.replace(/^\//, ""));
  const scope = request.headers?.get("x-zzz-scope") || "";
  const entityId = decodedPathname.substring(0, decodedPathname.length - extension.length).replace(/^\./, "");
  const context = searchParams.get("context") || searchParams.get("Context") || request.headers?.get("x-zzz-context") || "";
  const type = searchParams.get("type") || "Request";
  return {
    context,
    scope,
    entityId,
    extension,
    type,
  };
}
type RequestParts = {
  context: string;
  entityId: string;
  scope: string;
  extension: string;
  type: string;
};

// TODO: Tests
