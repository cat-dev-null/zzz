import { FeatureFlags } from "./app.ts";
import { Documentation } from "./modules/documentation/mod.ts";
import { Model } from "./stores/mod.ts";

export interface StringToStringMap {
  [key: string]: string;
}
// deno-lint-ignore no-explicit-any
export function asAny(thing: any): any {
  // deno-lint-ignore no-explicit-any
  return thing as any;
}
export class Action {
  features: FeatureFlags;
  env: StringToStringMap;
  constructor(features: FeatureFlags, env: StringToStringMap) {
    this.features = features;
    this.env = env;
  }
}
// deno-lint-ignore no-explicit-any
export function Meld(destination: any, source: any): void {
  if (!source) {
    return;
  }
  for (const key of Object.keys(source)) {
    if (key !== "Type" && key !== "Id") {
      if (destination[key] !== undefined && typeof destination[key] === "object") {
        Meld(destination[key], source[key]);
      } else {
        destination[key] = source[key];
      }
    }
  }
}
// deno-lint-ignore no-explicit-any
export function Log(...args: any[]): void {
  console.log.apply(null, args);
}
// deno-lint-ignore no-explicit-any
export function Trace(...args: any[]): void {
  if (Deno.args.includes("--trace") || Deno.env.get("VERBOSE") === "true") {
    Log(...args);
  }
}

export function getMarkdownForDocumentation(model: Model): string {
  const lines = [];
  const docs = asAny(model).Documentation as Documentation;
  if (docs.Description) {
    lines.push("## Description:");
    lines.push("*" + docs.Description + "*");
  }
  if (docs.Tags) {
    lines.push("## Tags:");
    docs.Tags.forEach((tag) => lines.push(" - " + tag));
  }
  if (docs.Parameters) {
    lines.push("## Parameters:");
    lines.push("|     Param     |  Description  |");
    lines.push("| ------------- | ------------- |");
    Object.keys(docs.Parameters).forEach((param) => {
      lines.push("| " + param + " | " + docs.Parameters![param] + " |");
    });
  }
  if (docs.Response) {
    lines.push("## Response:");
    lines.push("```json");
    lines.push(docs.Response);
    lines.push("```");
  }
  if (docs.Comments) {
    lines.push("## Comments:");
    lines.push(docs.Comments);
  }
  return lines.join("\n");
}
